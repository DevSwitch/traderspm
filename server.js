var express = require('express');
var mongoose = require('mongoose');
var passport = require('passport');
var flash = require('connect-flash');
var morgan = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var session = require('express-session');
var fs = require('fs');
var path = require('path');
var CoinPayments = require('coinpayments');

var app = express();
var port = process.env.PORT || 80;

var configDB = require('./config/database.js');
mongoose.connect(configDB.url); 
mongoose.set('debug',true);

require('./config/passport')(passport); 

app.use(express.static(__dirname + '/static'))

var accessLogStream = fs.createWriteStream(path.join(__dirname, 'access.log'), {flags: 'a'})
app.use(morgan('dev', {stream: accessLogStream}))

app.use(cookieParser()); 
app.use(bodyParser());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.set('views', __dirname + '/views');
app.set('view engine', 'ejs'); 

app.use(session({ secret: '@user@!oca!M@Y!ike$M$' })); 
app.use(passport.initialize());
app.use(passport.session()); 
app.use(flash()); 

require('./app/routes.js')(app, passport); 

app.listen(port);
console.log('The magic happens on port ' + port);