var User = require('../app/models/user');
var fs = require('fs');
var twilio = require('twilio');
var bittrex = require('node.bittrex.api');
var CoinPayments = require('coinpayments');
var settings = require('../settings.json');
var whenAmI = new Date().toLocaleTimeString();

let events = CoinPayments.events;

let middleware = [
	CoinPayments.ipn({
	  'merchantId': settings.COINPAYMENTS_MERCHANT_ID,
	  'merchantSecret': settings.COINPAYMENTS_MERCHANT_SECRET
	}),
	function (req, res, next) {
		console.log(req.body);
}];

module.exports = function(app, passport) {
  app.get('/', function(req, res) {
      res.render('index.ejs');
  });

	app.get('/auth/google',
		passport.authenticate('google', {
			scope : ['profile', 'email']
	}));

  app.get('/auth/google/callback',
		passport.authenticate('google', {
			successRedirect : '/profile',
			failureRedirect : '/'
	}));

	app.post('/auth/coinpayments', middleware, function(data){
		console.log(data);
		console.log('#####');
	});

  app.get('/profile', isLoggedIn, function(req, res) {
		res.render('profile.ejs', {
			user : req.user
		});
		if(req.user.key != '') {
			setInterval(function() {
				display(req.user.key,req.user.secret,req.user.google.id);
			}, 300000);
			display(req.user.key,req.user.secret,req.user.google.id);
		}
	});

  app.get('/logout', function(req, res) {
      req.logout();
      res.redirect('/');
  });

	app.post('/add', function(req, res) {
		User.findOne({  'google.id' : req.body.gid },function(err, doc) {
			User.findByIdAndUpdate(doc._id, {$set: { number: req.body.number, key: req.body.key, secret: req.body.secret }}, {'new': true}, function (err, result) {
				if (err) { res.send('There was a problem adding the information to the database.') }
				else { res.redirect('/profile'); }
			});
		});
	});
};

events.on('ipn_fail', function(data){
	console.log('IPN FAIL: ' + data.txn_id + ' : ' + data.email);
	User.findOne({  'google.email' : data.email },function(err, doc) {
		User.findByIdAndUpdate(doc._id, {$set: { vip: 'Failed' }}, {'new': true}, function (err, result) {
			if (err) { res.send('There was a problem updating your VIP status. Please contact admin@bitdiff.net with the following transaction ID: ' + data.txn_id + '.') }
		});
	});
});

events.on('ipn_pending', function(data){
	console.log('IPN PENDING: ' + data.txn_id + ' : ' + data.email);
	User.findOne({  'google.email' : data.email },function(err, doc) {
		User.findByIdAndUpdate(doc._id, {$set: { vip: 'Pending' }}, {'new': true}, function (err, result) {
			if (err) { res.send('There was a problem updating your VIP status. Please contact admin@bitdiff.net with the following transaction ID: ' + data.txn_id + '.') }
		});
	});
});

events.on('ipn_complete', function(data){
	console.log('IPN COMPLETE: ' + data.txn_id + ' : ' + data.email);
	User.findOne({  'google.email' : data.email },function(err, doc) {
		User.findByIdAndUpdate(doc._id, {$set: { vip: 'Purchased' }}, {'new': true}, function (err, result) {
			if (err) { res.send('There was a problem updating your VIP status. Please contact admin@bitdiff.net with the following transaction ID: ' + data.txn_id + '.') }
		});
	});
});

function isLoggedIn(req, res, next) {
    if (req.isAuthenticated())
        return next();
    res.redirect('/');
}

function notify(BITTREX_API,BITTREX_SECRET,to_num,gid){
	var date = new Date();
	var ACCOUNT_SID = settings.ACCOUNT_SID;
	var AUTH_TOKEN = settings.AUTH_TOKEN;
	var from_num = settings.FROM_NUM;
	var client = new twilio(ACCOUNT_SID, AUTH_TOKEN);

	bittrex.options({
		'apikey':BITTREX_API,
		'apisecret':BITTREX_SECRET,
	});

	function send(bs,to_num,coin_num,coin_name,closed,total) {
		message = client.messages.create({
			to:to_num,
			from:from_num,
			body:"This is a message from Traders.pm.\nYour "+bs+" order of "+coin_num+" "+coin_name+" closed at "+closed+" for a total of "+total+"!"}).then((message)=>console.log(message.sid))
	}

	var orders = new Array();
	bittrex.getorderhistory({depth:100},function(data,err){
		for(i=0;i<Object.keys(data.result).length;i++){
			var todayDate = new Date().toJSON().slice(0,10)
			var orderTime = data.result[i].TimeStamp.toString().slice(0,10)
			if(data.result[i].TimeStamp.toString().slice(0,10) === todayDate){
				open=data.result[i].OrderUuid;
				orders.push(open);
			}
		}
		for(i=0;i<orders.length;i++){
			bittrex.getorder({uuid: orders[i]},function(data,err){
				if(!err){
					var NOW = new Date().getTime();
					var TIME = new Date(data.result.Closed).getTime();
					var FIVE = 60 * 5 * 1000;
					if(data.result.Closed != null && ((NOW - TIME) < FIVE)){
						//Move into its own loop and check its length
						//console.log(data.result.Type + ' '  + to_num + ' '  + data.result.Quantity + ' '  + data.result.Exchange + ' '  + data.result.Closed + ' '  + data.result.Price);
						send(data.result.Type,to_num,data.result.Quantity,data.result.Exchange,data.result.Closed,data.result.Price);
						var senddetailLog = to_num + ' '  + data.result.Type + ' '  + data.result.Quantity + ' '  + data.result.Exchange + ' '  + data.result.Closed + ' '  + data.result.Price;
						fs.appendFile('tradersvipsendlog.txt', senddetailLog + '\n', function (err) {
							if (err) console.log(err);
							else console.log(whenAmI + ' ' + senddetailLog + ' > tradersvipsendlog.txt');
						});
					}
				} else {console.log(err)}
			})
		}
	})
}

function display(BITTREX_API,BITTREX_SECRET,gid){
	var date = new Date();

	bittrex.options({
		'apikey':BITTREX_API,
		'apisecret':BITTREX_SECRET,
	});

	var cOrders = new Array();
	var clOrders = new Array();
	bittrex.getorderhistory({depth:100},function(data,err){
		for(i=0;i<Object.keys(data.result).length;i++){
			var todayDate = new Date().toJSON().slice(0,10);
			if(data.result[i].TimeStamp.toString().slice(0,10) === todayDate){
				open=data.result[i].OrderUuid;
				cOrders.push(open);
			}
		}
		for(i=0;i<cOrders.length;i++){
			bittrex.getorder({uuid: cOrders[i]},function(data,err){
				var updateCont = data.result.Type + ' ' + data.result.Quantity + ' ' + data.result.Exchange + ' ' + data.result.Closed.toString().slice(0,10) + ' ' + data.result.Closed.toString().slice(12,19) + ' ' + data.result.Price;
				clOrders.push(updateCont);
			});
		}
		setTimeout(function() {
			User.findOne({  'google.id' : gid },function(err, docs) {
				User.findByIdAndUpdate(docs._id, {$set: { cOrders: clOrders }}, {'new': true}, function (err, result) {
					if (err) { console.log('There was a problem adding the information to the database.') }
				});
			});
		}, 500);

	});

	var oOrders = new Array();
	var o1Orders = new Array();
	bittrex.getopenorders({depth:100},function(data,err){
		for(i=0;i<Object.keys(data.result).length;i++){
			var todayDate = new Date().toJSON().slice(0,10);
			var openDate = data.result[i].Opened.toString().slice(0,10);
			if(data.result[i].Opened.toString().slice(0,10) === todayDate){
				oOrder=data.result[i]
				oOrders.push(oOrder)
			}
		}
		for(i=0;i<oOrders.length;i++){
			current=oOrders[i]
			var updateCont = current.OrderType + ' ' + current.Quantity + ' ' + current.Exchange + ' ' + current.Price + ' ' + current.Opened;
			o1Orders.push(updateCont);
		}
		setTimeout(function() {
			User.findOne({  'google.id' : gid },function(err, docs) {
				User.findByIdAndUpdate(docs._id, {$set: { pOrders: o1Orders }}, {'new': true}, function (err, result) {
					if (err) { console.log('There was a problem adding the information to the database.') }
				});
			});
		}, 500);
	})
}
/*
function vipProc(){
	User.find({  'vip' : 'Purchased' },function(err, doc) {
		//var whenAmI = new Date().toLocaleTimeString();
		if(doc.length > 0) console.log(whenAmI + ' - VIP Users Found!')
		else console.log(whenAmI + ' - No VIP Users Found.')
		if (err) console.log('There was a problem.' + err)
		else {
			for(let i = 0, l = doc.length; i < l; i++) {
				notify(doc[i].key,doc[i].secret,doc[i].number,doc[i].google.id);
			}
		}
	});
	//setInterval(vipProc, 300000);
}
vipProc();
setInterval(vipProc, 300000);
*/
